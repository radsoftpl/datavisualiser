<?php

namespace RadSoft;

class ShowMenu
{
    const TEMPLATE =<<<HTML
<!doctype html>
<html lang="en">
<head>
<title>Data Visualiser</title>
<style type="text/css">
* { padding: 5px; }
blockquote::after { margin-top: 12px; }
blockquote::after, blockquote::before { background-image: linear-gradient(90deg,#005282,#83bfff); content: ""; display: block; height: 5px; margin-bottom: 12px; }
</style>
</head>
<body>
    <h1 style="text-align: center;">Data Visualiser menu:</h1>
<form method="post" action="/">
    <input type="hidden" name="op" value="import">
<blockquote style="text-align: center;">
    <button type="submit">Data Reimport</button>
</blockquote>
</form>

<h3 style="text-align: center;"><!-- MESSAGE --></h3>

<form method="get" action="/">
<fieldset>
<legend>Set your params:</legend>
<table>
    <tr>
        <th>Select crawler:</th>
        <td>
            <select size="1" name="crawler">
<!-- CRAWLERS -->
            </select>
        </td>
        <th>Starting date:</th>
        <td>
            <select size="1" name="from">
<!-- DATE_FROM -->
            </select>
        </td>
        <th>Ending date:</th>
        <td>
            <select size="1" name="to">
<!-- DATE_TO -->
            </select>
        </td>
        <th>Draw graphs:</th>
        <td>
            <select name="op">
<option value="draw_all">Draw all graphs</option>
<option value="categories">Categories</option>
<option value="prices">Prices</option>
<!--<option value="products_changed">Products changed</option>-->
<!--<option value="products_downloads">Products downloads</option>-->
<option value="relations">Relations</option>
<option value="searches">Searches</option>
            </select>
        </td>
        <td>
            <button type="submit" formtarget="_blank">Open in new window</button>
            <button type="submit" formtarget="_self">Open in this window</button>
        </td>
    </tr>
</table>
</fieldset>
</form>

</body>
</html>
HTML;


    public static function showMenu($json)
    {
        $msg = $_SESSION['msg'] ?? null;
        unset($_SESSION['msg']);
        $template = str_replace('<!-- MESSAGE -->', $msg, static::TEMPLATE);

        $crawlers = [];
        foreach ($json as $date => $crawlersList) {
            $secondArray = [];
            foreach ($crawlersList as $idx => $crawler) {
                $secondArray[] = $crawler['site'];
            }
            $crawlers = array_merge($crawlers, $secondArray);
        }
        asort($crawlers);
        $crawlers = array_unique($crawlers);
        $crawlersTemplate = '';
        foreach ($crawlers as $crawler) {
            $value = str_replace(['.', ' '], '_', $crawler);
            $crawlersTemplate .= '<option value="' . $value . '">' . $crawler . '</option>';
        }
        $template = str_replace('<!-- CRAWLERS -->', $crawlersTemplate, $template);

        $dates = array_keys($json);
        $template = str_replace('<!-- DATE_FROM -->', '<option>' . join('</option><option>', $dates) . '</option>', $template);
        $template = str_replace('<!-- DATE_TO -->', '<option>' . join('</option><option>', array_reverse($dates)) . '</option>', $template);

        die($template);
    }
}
