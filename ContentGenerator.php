<?php

namespace RadSoft;

class ContentGenerator
{
    const TEMPLATE = <<<HTML
<!doctype html>
<html lang="en">
<head>
<title><!-- TITLE --></title>
<style type="text/css">
.chart { height: 400px; }
</style>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
google.charts.load('current', {packages: ['corechart', 'line']});
google.charts.setOnLoadCallback(drawCharts);

function drawCharts() {
    <!-- CONSTRUCTOR -->
}
</script>
</head>
<body>
<h3 style="text-align: center;"><!-- TITLE --></h3>
<div id="categories"></div>
<div id="prices"></div>
<div id="products_changed"></div>
<div id="products_downloads"></div>
<div id="relations"></div>
<div id="searches"></div>
<div id="times_categories"></div>
<div id="times_prices"></div>
<div id="times_products"></div>
<div id="times_searches"></div>
<div id="times_total"></div>
</body>
</html>
HTML;

    const JS_TEMPLATE =<<<JS
var data<!-- UNAME --> = new google.visualization.DataTable();
data<!-- UNAME -->.addColumn('date', 'X');
data<!-- UNAME -->.addColumn('number', '<!-- UNAME -->');
<!-- ENABLE_TOOLTIPS -->
data<!-- UNAME -->.addRows([
<!-- DATATABLE -->
]);
var options<!-- UNAME --> = {title:'<!-- TITLE --> - <!-- UNAME --> count',hAxis:{title:'Date'},vAxis:{title:'Count'},legend:{position:'bottom'},colors:['<!-- COLOR -->'],tooltip:{isHtml:true}};
document.getElementById('<!-- LNAME -->').className = 'chart';
var chart<!-- UNAME --> = new google.visualization.LineChart(document.getElementById('<!-- LNAME -->'));
chart<!-- UNAME -->.draw(data<!-- UNAME -->, options<!-- UNAME -->);
JS;




    public static function showCharts(array $charts, $title)
    {
        $template = str_replace('<!-- CONSTRUCTOR -->', join($charts), static::TEMPLATE);
        $template = str_replace('<!-- TITLE -->', $title, $template);

        die ($template);
    }

    public static function getChartCategories(array $json, string $crawlerName, string $dateFrom, string $dateTo): string
    {
        $dataTable = self::generateDataTable($json, $crawlerName, $dateFrom, $dateTo, 'c');
        $result = str_replace(['<!-- UNAME -->', '<!-- LNAME -->'], ['Categories', 'categories'], self::JS_TEMPLATE);
        $result = str_replace('<!-- DATATABLE -->', $dataTable, $result);
        $result = str_replace('<!-- COLOR -->', '#4285f4', $result);

        return $result;
    }

    public static function getChartPrices(array $json, string $crawlerName, string $dateFrom, string $dateTo): string
    {
        $dataTable = self::generateDataTable($json, $crawlerName, $dateFrom, $dateTo, 'p');
        $result = str_replace(['<!-- UNAME -->', '<!-- LNAME -->'], ['Prices', 'prices'], self::JS_TEMPLATE);
        $result = str_replace('<!-- DATATABLE -->', $dataTable, $result);
        $result = str_replace('<!-- COLOR -->', '#db4437', $result);

        return $result;
    }

    public static function getChartProductsChanged(array $json, string $crawlerName, string $dateFrom, string $dateTo): string
    {
        $dataTable = self::generateDataTable($json, $crawlerName, $dateFrom, $dateTo, 'pc');
        $result = str_replace(['<!-- UNAME -->', '<!-- LNAME -->'], ['ProductsChanged', 'products_changed'], self::JS_TEMPLATE);
        $result = str_replace('<!-- DATATABLE -->', $dataTable, $result);
        $result = str_replace('<!-- COLOR -->', '#097138', $result);

        return $result;
    }

    public static function getChartProductsDownloads(array $json, string $crawlerName, string $dateFrom, string $dateTo): string
    {
        $dataTable = self::generateDataTable($json, $crawlerName, $dateFrom, $dateTo, 'pd');
        $result = str_replace(['<!-- UNAME -->', '<!-- LNAME -->'], ['ProductsDownloads', 'products_downloads'], self::JS_TEMPLATE);
        $result = str_replace('<!-- DATATABLE -->', $dataTable, $result);
        $result = str_replace('<!-- COLOR -->', '#c26ff4', $result);

        return $result;
    }

    public static function getChartRelations(array $json, string $crawlerName, string $dateFrom, string $dateTo): string
    {
        $dataTable = self::generateDataTable($json, $crawlerName, $dateFrom, $dateTo, 'r');
        $result = str_replace(['<!-- UNAME -->', '<!-- LNAME -->'], ['Relations', 'relations'], self::JS_TEMPLATE);
        $result = str_replace('<!-- DATATABLE -->', $dataTable, $result);
        $result = str_replace('<!-- COLOR -->', '#00f', $result);

        return $result;
    }

    public static function getChartSearches(array $json, string $crawlerName, string $dateFrom, string $dateTo): string
    {
        $dataTable = self::generateDataTable($json, $crawlerName, $dateFrom, $dateTo, 's');
        $result = str_replace(['<!-- UNAME -->', '<!-- LNAME -->'], ['Searches', 'searches'], self::JS_TEMPLATE);
        $result = str_replace('<!-- DATATABLE -->', $dataTable, $result);
        $result = str_replace('<!-- COLOR -->', '#f4b400', $result);

        return $result;
    }

    public static function getChartTimesCategories(array $json, string $crawlerName, string $dateFrom, string $dateTo): string
    {
        $dataTable = self::generateDataTable($json, $crawlerName, $dateFrom, $dateTo, 'ct');
        $result = str_replace(['<!-- UNAME -->', '<!-- LNAME -->'], ['CategoriesTimes', 'times_categories'], self::JS_TEMPLATE);
        $result = self::enableTooltips('CategoriesTimes', $result);
        $result = str_replace('<!-- DATATABLE -->', $dataTable, $result);
        $result = str_replace('<!-- COLOR -->', '#8A2BE2', $result);

        return $result;
    }

    public static function getChartTimesPrices(array $json, string $crawlerName, string $dateFrom, string $dateTo): string
    {
        $dataTable = self::generateDataTable($json, $crawlerName, $dateFrom, $dateTo, 'pt');
        $result = str_replace(['<!-- UNAME -->', '<!-- LNAME -->'], ['PricesTimes', 'times_prices'], self::JS_TEMPLATE);
        $result = self::enableTooltips('PricesTimes', $result);
        $result = str_replace('<!-- DATATABLE -->', $dataTable, $result);
        $result = str_replace('<!-- COLOR -->', '#D2691E', $result);

        return $result;
    }

    public static function getChartTimesProducts(array $json, string $crawlerName, string $dateFrom, string $dateTo): string
    {
        $dataTable = self::generateDataTable($json, $crawlerName, $dateFrom, $dateTo, 'rt');
        $result = str_replace(['<!-- UNAME -->', '<!-- LNAME -->'], ['ProductsTimes', 'times_products'], self::JS_TEMPLATE);
        $result = self::enableTooltips('ProductsTimes', $result);
        $result = str_replace('<!-- DATATABLE -->', $dataTable, $result);
        $result = str_replace('<!-- COLOR -->', '#0ff', $result);

        return $result;
    }

    public static function getChartTimesSearches(array $json, string $crawlerName, string $dateFrom, string $dateTo): string
    {
        $dataTable = self::generateDataTable($json, $crawlerName, $dateFrom, $dateTo, 'st');
        $result = str_replace(['<!-- UNAME -->', '<!-- LNAME -->'], ['SearchesTimes', 'times_searches'], self::JS_TEMPLATE);
        $result = self::enableTooltips('SearchesTimes', $result);
        $result = str_replace('<!-- DATATABLE -->', $dataTable, $result);
        $result = str_replace('<!-- COLOR -->', '#006400', $result);

        return $result;
    }

    public static function getChartTimesTotal(array $json, string $crawlerName, string $dateFrom, string $dateTo): string
    {
        $dataTable = self::generateDataTable($json, $crawlerName, $dateFrom, $dateTo, 'tt');
        $result = str_replace(['<!-- UNAME -->', '<!-- LNAME -->'], ['TotalTimes', 'times_total'], self::JS_TEMPLATE);
        $result = self::enableTooltips('TotalTimes', $result);
        $result = str_replace('<!-- DATATABLE -->', $dataTable, $result);
        $result = str_replace('<!-- COLOR -->', '#FFD700', $result);

        return $result;
    }

    private static function generateDataTable(array $json, string $crawlerName, string $dateFrom, string $dateTo, $crawlerDataIndex): string
    {
        $dataTable = '';
        $loop = 0;
        foreach ($json as $date => $crawlers) {
            if ($date >= $dateFrom && $date <= $dateTo) {
                $value = $json["$date"][$crawlerName][$crawlerDataIndex] ?? null;
                if (isset($value)) {
                    $dateFormatted = substr($date, 0, 4) . '-' . substr($date, 4, 2) . '-' . substr($date, -2);
                    if (strlen($dataTable)) {
                        $dataTable .= ',';
                    }
                    if ($loop == 10) {
                        $dataTable .= "\r\n";
                    }
                    $tooltip = '';
                    if (in_array($crawlerDataIndex, ['ct', 'pt', 'rt', 'st', 'tt'])) {
                        $hours = floor($value / 60 / 60);
                        $tmpValue = $value - ($hours * 60 * 60);
                        $minutes = floor($tmpValue / 60);
                        $seconds = $tmpValue - ($minutes * 60);
                        $tooltip = ", '<p style=\"font-size: 1.5em; margin: 10px;\"><b>{$dateFormatted}</b></br>{$hours} godzin, {$minutes} minut, {$seconds} sekund</p>'";
                    }

                    $dataTable .= '[new Date("' . $dateFormatted . '"), ' . $value . $tooltip . ']';
                    $loop++;
                }
            }
        }

        return $dataTable;
    }

    private static function enableTooltips($uname, $template)
    {
        $result = str_replace('<!-- UNAME -->', $uname, "data<!-- UNAME -->.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});");
        $result = str_replace('<!-- ENABLE_TOOLTIPS -->', $result, $template);

        return $result;
    }
}
