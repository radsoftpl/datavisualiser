<?php

namespace RadSoft;

require_once 'Application.php';

error_reporting(E_ALL ^ E_WARNING);

session_start();
$app = Application::getInstance();
$app->run();
