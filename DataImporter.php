<?php

namespace RadSoft;

class DataImporter
{
    const EMPTY = '';

    protected $basicDataColumns = [
        'Site' => 'site',
        'Categories' => 'c',
        'Prices' => 'p',
//            'Products changed' => 'pc',
//            'Products downloads' => 'pd',
        'Relations' => 'r',
        'Search' => 's',
    ];

    protected $timeRelatedColumns = [
        'Site' => 'site',
        'Categories time' => 'ct',
        'Prices time' => 'pt',
        'Products time' => 'rt',
        'Search time' => 'st',
        'Total time' => 'tt',
    ];

    public function importData(): array
    {
        $data = [];
        $dir = __DIR__ . '/data/basic';
        $files = scandir($dir);
        if (is_array($files)) {
            foreach ($files as $file) {
                $pathInfo = pathinfo($file);
                if (strtolower($pathInfo['extension'] ?? static::EMPTY) == 'csv') {
                    $fileName = $pathInfo['filename'];
                    $fileDataBasic = $this->importFile("{$dir}/{$pathInfo['basename']}", $this->basicDataColumns);
                    $fileDataTimes = $this->importFile("{$dir}/{$pathInfo['basename']}", $this->timeRelatedColumns);
                    $data['basic'][$fileName] = $fileDataBasic;
                    $data['times'][$fileName] = $fileDataTimes;
                }
            }
        }

        return $data;
    }

    protected function importFile(string $fileName, array $columnsToImport): array
    {
        $results = [];
        $columnsToImportKeys = array_keys($columnsToImport);
        $fh = fopen($fileName, 'r');
        if ($fh !== false) {
            $row = 0;
            $columnsInFile = [];
            while (($line = fgetcsv($fh)) !== false) {
                if ($row == 0) {
                    $columnsInFile = $line;
                    $columnsInFile[0] = $this->removeBom($columnsInFile[0]);
                } else {
                    $result = [];
                    foreach ($line as $idx => $value) {
                        if (in_array($columnsInFile[$idx], $columnsToImportKeys)) {
                            $result[$columnsToImport[$columnsInFile[$idx]]] = trim($this->removeBom($value));
                        }
                    }
                    $crawlerId = str_replace(['.', ' '], '_', $result['site']);
                    $results[$crawlerId] = $result;
                }
                $row++;
            }
            fclose($fh);
        }

        return $results;
    }

    private function removeBom($string)
    {
        if (strpos(bin2hex($string), 'efbbbf') === 0) {
            return substr($string, 3);
        }

        return $string;
    }
}
