<?php

namespace RadSoft;

class Application
{
    const IMPORT_FILE_BASIC = './data/data.json';
    const IMPORT_FILE_TIMES = './data/times.json';
    const IMPORT_FILE_LOCATIONS = './data/locations.json';

    /** @var Application */
    private static $instance = null;

    public static function getInstance()
    {
        if (! isset(static::$instance)) {
            static::$instance = new Application();
        }
        return static::$instance;
    }

    private function __construct()
    {
        $files = [
            './ContentGenerator.php',
            './DataImporter.php',
            './ShowMenu.php',
        ];

        foreach ($files as $file) {
            require_once $file;
        }
    }

    public function run()
    {
        $json = [];
        $json['basic'] = $this->loadFile(self::IMPORT_FILE_BASIC);
        $json['times'] = $this->loadFile(self::IMPORT_FILE_TIMES);

        if (isset($_POST['op']) && $_POST['op'] == 'import') {
            $fh1 = fopen(self::IMPORT_FILE_BASIC, 'w');
            $fh2 = fopen(self::IMPORT_FILE_TIMES, 'w');
//            $fh3 = fopen(self::IMPORT_FILE_LOCATIONS, 'w');
            if ($fh1 === false || $fh2 === false /*|| $fh3 === false*/) {
                die ("<h2>Could not open file for writing</h2><p>" . __FILE__ . ": " . __LINE__ . "</p>");
            }
            $di = new DataImporter();
            $data = $di->importData();
            fwrite($fh1, json_encode($data['basic']));
            fwrite($fh2, json_encode($data['times']));
            fclose($fh1);
            fclose($fh2);
            $_SESSION['msg'] = 'CSV Data has been imported';
            $url = "http://{$_SERVER['HTTP_HOST']}/";
            header ("Location: {$url}");
            die;
        }

        $charts = [];
        $operation = $_GET['op'] ?? null;
        $crawler = $_GET['crawler'] ?? null;
        $from = $_GET['from'] ?? null;
        $to = $_GET['to'] ?? null;
        if (isset($operation) && !isset($crawler, $from, $to)) {
            $_SESSION['msg'] = 'Missing required params!';
            $operation = 'menu';
        } else {
            $crawlerName = '';
            foreach ($json['basic'] as $date => $crawlers) {
                if (isset($crawlers[$crawler])) {
                    $crawlerName = $crawlers[$crawler]['site'];
                    break;
                }
            }
            $title = str_replace('\'', '&#39;', $crawlerName) . ' - charts for dates ' . $this->formatDate($from) . ' - ' . $this->formatDate($to);
        }

        switch($operation) {
            case 'draw_all':
                $charts[] = ContentGenerator::getChartCategories($json['basic'], $crawler, $from, $to);
                $charts[] = ContentGenerator::getChartPrices($json['basic'], $crawler, $from, $to);
//                $charts[] = ContentGenerator::getChartProductsChanged($json['basic'], $crawler, $from, $to);
//                $charts[] = ContentGenerator::getChartProductsDownloads($json['basic'], $crawler, $from, $to);
                $charts[] = ContentGenerator::getChartRelations($json['basic'], $crawler, $from, $to);
                $charts[] = ContentGenerator::getChartSearches($json['basic'], $crawler, $from, $to);
                $charts[] = ContentGenerator::getChartTimesCategories($json['times'], $crawler, $from, $to);
                $charts[] = ContentGenerator::getChartTimesPrices($json['times'], $crawler, $from, $to);
                $charts[] = ContentGenerator::getChartTimesProducts($json['times'], $crawler, $from, $to);
                $charts[] = ContentGenerator::getChartTimesSearches($json['times'], $crawler, $from, $to);
                $charts[] = ContentGenerator::getChartTimesTotal($json['times'], $crawler, $from, $to);
                break;
            case 'categories':
                $charts[] = ContentGenerator::getChartCategories($json['basic'], $crawler, $from, $to);
                break;
            case 'prices':
                $charts[] = ContentGenerator::getChartPrices($json['basic'], $crawler, $from, $to);
                break;
//            case 'products_changed':
//                $charts[] = ContentGenerator::getChartProductsChanged($json['basic'], $crawler, $from, $to);
//                break;
//            case 'products_downloads':
//                $charts[] = ContentGenerator::getChartProductsDownloads($json['basic'], $crawler, $from, $to);
//                break;
            case 'relations':
                $charts[] = ContentGenerator::getChartRelations($json['basic'], $crawler, $from, $to);
                break;
            case 'searches':
                $charts[] = ContentGenerator::getChartSearches($json['basic'], $crawler, $from, $to);
                break;
            default:
                ShowMenu::showMenu($json['basic']);
                break;
        }

        ContentGenerator::showCharts($charts, $title);
    }

    private function formatDate(?string $date)
    {
        if (strlen($date) == 8) {
            return substr($date, 0, 4) . '-' . substr($date, 4, 2) . '-' . substr($date, -2);
        }

        return null;
    }

    private function loadFile(string $fileName)
    {
        $result = [];
        $fh = fopen($fileName, 'r');
        if ($fh !== false) {
            fseek($fh, 0);
            $result = json_decode(fread($fh, filesize($fileName)), true);
            fclose($fh);
        }

        return $result;
    }
}
