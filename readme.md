# Data Visualiser

### How to run?

`php -S localhost:8080`

Then open browser on selected port: http://localhost:8080/

### How to feed it with data?

1) Open Kibana Report "Prices / Products / Categories per shop":
[Report in Kibana][https://139.162.179.38:5602/app/kibana#/visualize/edit/25ffde90-71e3-11e7-a3c8-c76549eddc82]

2) Save "Raw" data-dump (button at the bottom of the data-table) into the "./data/basic/" directory with filename "YYYYmmdd.csv" where YYYY is the year, mm is 2-digit month number and dd is 2-digit day number.

3) Open Homepage and click "Data reimport". All the CSV files inside "./data/" directory will be parsed and new file "./data/data.json" will be created / overwritten if existing.

![image](./charts.png)


[https://139.162.179.38:5602/app/kibana#/visualize/edit/25ffde90-71e3-11e7-a3c8-c76549eddc82]: https://139.162.179.38:5602/app/kibana#/visualize/edit/25ffde90-71e3-11e7-a3c8-c76549eddc82
